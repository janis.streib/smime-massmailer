# smime-massmailer

`smime-massmailer` intends to be a toolbox for generating signed and encrypted
mails.

It currently features one such tool:

## smime-encryption-testmailer

Given a number of x509-certificates, `smime-encryption-testmailer` will
generate one encrypted e-mail per certificate for all mail addresses. Its
primary use case is remote debugging of client problems for users with
(potentially multiple) certificates.

See `smime-encryption-testmailer -help` for usage details.

# Need a working tool to send signed emails to a large number of people?

Try [this project (written in python)](https://git.scc.kit.edu/KIT-CERT/massmailer).
