package smime_massmailer

import (
	"bytes"
	"fmt"
	"os/exec"
)

func Openssl(stdin []byte, args ...string) ([]byte, error) {
	opensslpath, err := exec.LookPath("openssl")
	if err != nil {
		return []byte{}, err
	}
	cmd := exec.Command(opensslpath, args...)

	in := bytes.NewReader(stdin)
	out := &bytes.Buffer{}
	errs := &bytes.Buffer{}

	cmd.Stdin, cmd.Stdout, cmd.Stderr = in, out, errs

	if err := cmd.Run(); err != nil {
		if len(errs.Bytes()) > 0 {
			return nil, fmt.Errorf("error running %s (%s):\n %v", cmd.Args, err, errs.String())
		}
		return nil, err
	}

	return out.Bytes(), nil
}
