module git.scc.kit.edu/heiko.reese/smime-massmailer/v2

go 1.16

require (
	git.scc.kit.edu/heiko.reese/smime-massmailer v0.0.0-20210604134203-699284cb9a2f
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
