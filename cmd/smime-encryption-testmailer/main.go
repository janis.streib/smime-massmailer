package main

import (
	"crypto/x509"
	"encoding/pem"
	"flag"
	"io/ioutil"
	"log"
	"strings"
	"text/template"

	"bytes"
	"fmt"
	"os"

	"time"

	"git.scc.kit.edu/heiko.reese/smime-massmailer"
	"gopkg.in/gomail.v2"
)

const (
	emailBodyTemplateKITCA = `Content-Type: text/plain; charset=utf-8
Content-Language: de-DE
Content-Transfer-Encoding: binarymime

Hallo {{ .Cert.Subject.CommonName }},

diese E-Mail wurde mit folgendem Zertifikat verschlüsselt:

  Seriennummer: {{ .Cert.SerialNumber.String }} (0x{{ .Cert.SerialNumber.Text 16 }})
  CommonName:   {{ .Cert.Subject.CommonName }}
  {{- if gt (len .Cert.Subject.OrganizationalUnit) 0}}
  OU:           {{ StringsJoin .Cert.Subject.OrganizationalUnit ", " }}
  {{- end}}
  Gültig ab:    {{ .Cert.NotBefore.Local }}
  Gültig bis:   {{ .Cert.NotAfter.Local }}
  {{- if lt (len .Cert.EmailAddresses) 2}}
  E-Mail:       {{ StringsJoin .Cert.EmailAddresses ", " }}
  {{- else }}
  E-Mails:      {{ StringsJoin .Cert.EmailAddresses ", " }}
  {{- end }}

Viele Grüße,
  Ihre KIT-CA

--
KIT-CA, Steinbuch Centre for Computing, KIT, Germany
ca@kit.edu | Phone: +49 721 608-45678 | https://www.ca.kit.edu
`
)

type Config struct {
	bodyfile, htmlfile string
	recipient, sender  string
	dump, send         bool
	mailhost           string
	mailport           int
}

type TemplateData struct {
	Cert *x509.Certificate
}

var (
	appconfig Config
)

func init() {
	flag.StringVar(&appconfig.bodyfile, "body", "body.template", "File containing email body template (optional)")
	flag.StringVar(&appconfig.htmlfile, "html", "html.template", "File containing email html body template (optional)")
	flag.StringVar(&appconfig.sender, "sender", "KIT-CA <ca@kit.edu>", "Specify sender")
	flag.StringVar(&appconfig.recipient, "recipient", "", "Alternate recipient (optional, will use email from certificate if not specified)")
	flag.BoolVar(&appconfig.dump, "dump", false, "Write emails to files (default: false)")
	flag.BoolVar(&appconfig.send, "send", true, "Send emails (default: true)")
	flag.StringVar(&appconfig.mailhost, "mailhost", "smarthost.kit.edu", "Mailserver hostname")
	flag.IntVar(&appconfig.mailport, "port", 25, "Mailserver port")
	flag.Parse()

	// use AES-128 instead of 3DES (currently broken!)
	//pkcs7.ContentEncryptionAlgorithm = pkcs7.EncryptionAlgorithmAES128GCM
}

func main() {
	var (
		err             error
		RawBodyTemplate []byte
		BodyWriter      bytes.Buffer
		mailserver      *gomail.Dialer
		localhost       string
	)
	// configure mailhost if needed
	if appconfig.send {
		localhost, err = os.Hostname()
		if err != nil {
			localhost = "localhorst" // this is on purpose
		}
		mailserver = gomail.NewDialer(appconfig.mailhost, appconfig.mailport, "", "")
		mailserver.LocalName = localhost
	}
	// check if certificates were specified
	if flag.NArg() < 1 {
		log.Fatal("Please specify at least one certificate")
	}
	// prepare body template
	RawBodyTemplate, err = ioutil.ReadFile(appconfig.bodyfile)
	if err != nil {
		log.Printf("Unable to read body template %s; falling back to builtin default", appconfig.bodyfile)
		// fall back to default template
		RawBodyTemplate = []byte(emailBodyTemplateKITCA)
	}
	BodyTemplate := template.New("mailbody")
	BodyTemplate = BodyTemplate.Funcs(template.FuncMap{"StringsJoin": strings.Join})
	BodyTemplate, err = BodyTemplate.Parse(string(RawBodyTemplate[:]))
	if err != nil {
		log.Fatalf("Error parsing body template: %s", err)
	}

	// read and parse all certificates
	certs := ReadCertificates(flag.Args()...)
	if len(certs) < 1 {
		log.Fatal("No valid certificates found")
	}

	// generate a single email per certificate
	for filename, currentCerts := range certs {
		for _, cert := range currentCerts {
			// new email message
			message := gomail.NewMessage()

			// Set recipient(s)
			recipients := make([]string, 0)
			if strings.Contains(appconfig.recipient, "@") {
				recipients = append(recipients, appconfig.recipient)
			} else {
				if len(cert.EmailAddresses) < 1 {
					log.Fatalf("Certificate %s does not contain email adresses", cert.SerialNumber.String())
				}
				for _, recipient := range cert.EmailAddresses {
					recipients = append(recipients, recipient)
				}
			}
			for _, recipient := range recipients {
				message.SetAddressHeader("To", recipient, cert.Subject.CommonName)
			}
			// set sender
			message.SetHeader("From", appconfig.sender)
			message.SetHeader("Reply-To", appconfig.sender)

			// set subject
			subject := fmt.Sprintf("Verschlüsselt mit Zertifikat %s (CN: %s); gültig vom %s bis %s",
				cert.SerialNumber.String(),
				cert.Subject.CommonName,
				cert.NotBefore.In(time.Local).Format("2.1.2006"),
				cert.NotAfter.In(time.Local).Format("2.1.2006"))
			message.SetHeader("Subject", subject)

			BodyWriter.Reset()
			// write body, execute template
			BodyTemplate.Execute(&BodyWriter, TemplateData{cert})

			// call openssl
			encryptedPart, err := smime_massmailer.Openssl(BodyWriter.Bytes(),
				"cms",
				"-encrypt",
				"-outform",
				"DER",
				"-md",
				"SHA256",
				"-aes128",
				filename)
			if err != nil {
				log.Fatal(err)
			}

			// add encrypted body
			message.SetBody(`application/pkcs7-mime; name="smime.p7m"; smime-type=enveloped-data`,
				//base64.StdEncoding.EncodeToString(encryptedPart[:]),
				//strings.Replace(string(encryptedPart[:]), "\n", "\r\n", -1),
				string(encryptedPart[:]),
				gomail.SetPartEncoding(gomail.Base64))

			// write output
			if appconfig.dump {
				filename := fmt.Sprintf("%s - %s.eml", strings.Join(recipients, ","), cert.SerialNumber.String())
				f, err := os.OpenFile(filename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
				if err != nil {
					log.Fatal(err)
				}
				defer f.Close()
				_, err = message.WriteTo(f)
				if err != nil {
					log.Fatal(err)
				} else {
					log.Printf("Wrote %s\n", filename)
				}
			}

			if appconfig.send {
				err = mailserver.DialAndSend(message)
				if err != nil {
					log.Fatal(err)
				} else {
					log.Printf("Sent e-mail for %s to %s", cert.SerialNumber.String(), strings.Join(recipients, ", "))
				}

			}
		}
	}
}

// ReadCertificates reads alls x509 certificates from a list of input files. Errors are logged and skipped.
func ReadCertificates(filenames ...string) map[string][]*x509.Certificate {
	var (
		allcerts = make(map[string][]*x509.Certificate)
		block    *pem.Block
	)
	for _, filename := range filenames {
		// read input file
		content, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Printf("Error reading file %s: %s", filename, err)
			continue
		}
		// try decoding as DER
		certs, err := x509.ParseCertificates(content)
		if err == nil {
			// found certs: append and go to next file
			allcerts[filename] = append(allcerts[filename], certs...)
			continue
		}
		// try decoding as PEM
		for block, content = pem.Decode(content); block != nil; block, content = pem.Decode(content) {
			// process only certificates
			if block.Type == "CERTIFICATE" {
				certs, err := x509.ParseCertificates(block.Bytes)
				if err == nil {
					allcerts[filename] = append(allcerts[filename], certs...)
				}
			}
		}
	}
	return allcerts
}
