package smime_massmailer

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"syscall"
	"testing"
)

func FifoWriter(dir string, contents []byte) error {
	var (
		err error
		f   *os.File
	)
	// create new fifo
	fifoname := path.Join(dir, "secret")
	err = syscall.Mkfifo(fifoname, 0600)
	if err != nil {
		return err
	}
	// remove fifo after use
	defer os.Remove(fifoname)
	f, err = os.OpenFile(fifoname, os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer f.Close()

	source := bytes.NewReader(contents)
	_, err = io.Copy(f, source)
	return err
}

func TestFifo(t *testing.T) {
	dir, err := ioutil.TempDir("", "testing_fifos")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	fmt.Println(dir)

	FifoWriter(dir, []byte("This is a test!"))
}
